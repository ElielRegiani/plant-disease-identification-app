const images = {
    ferrugem: require('../assets/diseases/ferrugem.jpg'),
    mancha: require('../assets/diseases/manchafoliar.jpg'),
    mildio: require('../assets/diseases/mildio.jpg'),
    mela: require('../assets/diseases/mela.jpeg'),
    podridao: require('../assets/diseases/podridaonegra.jpg'),
    saudavel: require('../assets/diseases/saudavel.jpg'),
  }
  
export { images };