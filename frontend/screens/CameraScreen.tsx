import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Modal, Image } from 'react-native';
import { Camera } from 'expo-camera';
import { FontAwesome } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { useIsFocused } from '@react-navigation/native';

export default function CameraScreen() {
  const camRef = useRef(null);
  const [hasPermission, setHasPermission] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [capturedPhoto, setCapturedPhoto] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const navigation = useNavigation();

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  if (hasPermission === null) {
    return <View />;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  async function takePicture(){
    if (camRef){
      const data = await camRef.current.takePictureAsync();
      setCapturedPhoto(data.uri);
      setOpenModal(true);
    }
  }

  return (
    <View style={styles.container}>
      <Camera style={styles.camera} type={type} ref={camRef}>
        <View style={styles.buttonContainer}>

          { capturedPhoto && 
            <Modal
              animationType="slide"
              transparent={false}
              visible={openModal}
            >
              <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', margin: 20}}>
                <Text style={{margin: 20, fontSize: 20, fontFamily: 'philosopher', color: '#002140'}}>Deseja prosseguir com a avaliação da foto retirada?</Text>
                <View style={{ flexDirection: 'row', margin: 10}}>
                  <TouchableOpacity style={{ margin: 10 }} onPress={() => setOpenModal(false)}>
                    <FontAwesome name="times-circle-o" size={50} color="#E00000"/>
                  </TouchableOpacity>
                  <TouchableOpacity style={{ margin: 10 }} onPress={() =>
                    
                    navigation.navigate('Diagnóstico', {
                      imageUri: capturedPhoto,
                    })
                  }>
                    <FontAwesome name="check-circle-o" size={50} color="#00C000"/>
                  </TouchableOpacity>
                </View>
                <Image
                  style={{ width: '100%', height: '50%', borderRadius: 20}}
                  source={{ uri: capturedPhoto }}
                />
              </View>
              
            </Modal>
          }
        </View>
        <View style={styles.cameraContainer}>
          <TouchableOpacity
          style={styles.flipButton}
              onPress={() => {
                setType(
                  type === Camera.Constants.Type.back
                    ? Camera.Constants.Type.front
                    : Camera.Constants.Type.back
                );
              }}>
              <FontAwesome name="refresh" size={30}  color="#FFF"/>
            </TouchableOpacity>
            <TouchableOpacity style={{}} onPress={ takePicture }>
              <FontAwesome name="camera" size={30} color="#FFF"/>
            </TouchableOpacity>
          </View>
      </Camera>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  camera: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    margin: 20,
  },
  button: {
    position: 'absolute',
    bottom: 20,
    left: 20,
  },
  cameraContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    height: 50,
  },
  flipButton:{
    marginRight: 150,
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
});
