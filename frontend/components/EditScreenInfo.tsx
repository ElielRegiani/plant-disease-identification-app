import { useNavigation } from '@react-navigation/native';
import * as WebBrowser from 'expo-web-browser';
import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Image, Modal, Alert, Pressable, Text } from 'react-native';
// import * as DocumentPicker from 'expo-document-picker';
import * as ImagePicker from 'expo-image-picker';

import { View } from './Themed';

export default function EditScreenInfo({ path }: { path: string }) {

  const navigation = useNavigation();
  const [modalVisible, setModalVisible] = useState(false);

  function fotografar(){
    setModalVisible(!modalVisible)
    navigation.navigate('Câmera')
  }

  async function utilizarFotoArmazenada(){

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    setModalVisible(!modalVisible)
    navigation.navigate('Diagnóstico', {
      imageUri: 'file://' + result.uri,
    })
  }
  
  return (
    <View>
      <View style={styles.getStartedContainer}>
      <Modal
        animationType="slide"
        visible={modalVisible}
        presentationStyle='fullScreen'
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Como deseja realizar o diagnóstico?</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => fotografar()}
            >
              <Text style={styles.textStyle}>Fotografar planta</Text>
            </Pressable>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => utilizarFotoArmazenada()}
            >
              <Text style={styles.textStyle}>Utilizar fotografia existente</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
      <TouchableOpacity onPress={() =>
        setModalVisible(true)
      }>
        <Image
          style={styles.tinyLogo}
          source={require("../assets/images/logo.png")}
        />
      </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  button: {
    backgroundColor: '#859a9b',
    borderRadius: 20,
    padding: 10,
    marginBottom: 20,
    shadowColor: '#303838',
    shadowOffset: { width: 0, height: 5 },
    shadowRadius: 10,
    shadowOpacity: 0.35,
  },
  tinyLogo: {
    width: 200,
    height: 200,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightContainer: {
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    lineHeight: 24,
    textAlign: 'center',
  },
  helpContainer: {
    marginTop: 15,
    marginHorizontal: 20,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
});
