export const weatherConditions = {
    Rain: {
      color: '#005BEA',
      title: 'Chuva',
      subtitle: 'Get a cup of coffee',
      icon: 'weather-rainy'
    },
    Clear: {
      color: '#f7b733',
      title: 'Ensolarado',
      subtitle: 'It is hurting my eyes',
      icon: 'weather-sunny'
    },
    Thunderstorm: {
      color: '#616161',
      title: 'Tempestade',
      subtitle: 'Because Gods are angry',
      icon: 'weather-lightning'
    },
    Clouds: {
      color: '#1F1C2C',
      title: 'Nublado',
      subtitle: 'Everywhere',
      icon: 'weather-cloudy'
    },
  
    Snow: {
      color: '#00d2ff',
      title: 'Neve',
      subtitle: 'Get out and build a snowman for me',
      icon: 'weather-snowy'
    },
    Drizzle: {
      color: '#076585',
      title: 'Chuva leve',
      subtitle: 'Partially raining...',
      icon: 'weather-hail'
    },
    Haze: {
      color: '#66A6FF',
      title: 'Chuva com neblina',
      subtitle: 'Another name for Partial Raining',
      icon: 'weather-hail'
    },
    Mist: {
      color: '#3CD3AD',
      title: 'Neblina',
      subtitle: "Don't roam in forests!",
      icon: 'weather-fog'
    }
  };