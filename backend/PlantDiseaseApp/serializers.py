from rest_framework import serializers

from .models import User, Request, Reply, PositioningInfo

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name']


class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = ['id', 'date', 'image']


class PositioningInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PositioningInfo
        fields = ['id', 'latitude', 'longitude', 'temperature', 'humidity', 'weather', 'city', 'state']


class ReplySerializer(serializers.ModelSerializer):
    class Meta:
        model = Reply
        fields = ['id', 'image', 'diseaseFirst', 'treatment', 'prevention', 'symptoms', 'additionalInformation', 'probabilityDiseaseFirst', 'probabilityDiseaseSecond', 'diseaseSecond']