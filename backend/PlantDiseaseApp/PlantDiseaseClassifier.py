import numpy as np
import os
import pickle
import cv2
import tensorflow as tf
from tensorflow import keras
from os import listdir
import tensorflow
from tensorflow import keras
from keras.models import load_model
from keras.preprocessing.image import img_to_array
from keras.models import model_from_json
import matplotlib.pyplot as plt

from manage import PROJECT_ROOT

# Dimension of resized image
default_image_size = tuple((224, 224))

def ConvertImageToArray(image_dir):
    try:
        image = plt.imread(os.path.join(PROJECT_ROOT, image_dir))
        if image is not None:
            image = cv2.resize(image, default_image_size)   
            return img_to_array(image)
        else:
            return np.array([])
    except Exception as e:
        print(f"Error : {e}")
        return None

    # load json and create model
def PrepareModelToPredict():
    json_file = open(os.path.join(PROJECT_ROOT, "PlantDiseaseApp/models/model_MobileNetV2_second.json"), "r")
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)

    # load weights into new model
    
    model.load_weights(os.path.join(PROJECT_ROOT, "PlantDiseaseApp/models/model_MobileNetV2_second.h5"))
    print("Loaded model from disk")
    
    return model

def PredictDisease(image_path, model):

    image_array = ConvertImageToArray(image_path)
    np_image = (np.array(image_array, dtype=np.float16) / 225.0)
    np_image = np.expand_dims(np_image,0)
    plt.imshow(plt.imread(os.path.join(PROJECT_ROOT, image_path)))
    classes = ['blight', 'healthy', 'mildew', 'rot', 'rust', 'spot']

    return model.predict(np_image)