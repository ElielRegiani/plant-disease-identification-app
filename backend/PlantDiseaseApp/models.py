from typing import Match
from django.db import models
from django.utils.translation import gettext_lazy as _

from PlantDiseaseApp.functions import upload_to

# Create your models here.

class User(models.Model):

    name = models.CharField(max_length=100)

    # Methods

    def __str__(self):
        return self.name

class Request(models.Model):

    date = models.DateField(null=True)
    image = models.ImageField(upload_to=upload_to)

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    # Methods

class PositioningInfo(models.Model):

    latitude = models.FloatField(null= True)
    longitude = models.FloatField(null= True)
    temperature = models.FloatField(null= True)
    humidity = models.FloatField(null= True)
    weather = models.CharField(max_length=50, null= True)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)

    user = models.OneToOneRel(User, field_name="", to='')

    # Methods

    def getCity(self):
        return self.city
    
    def getState(self):
        return self.state

class Reply(models.Model):
    
    image = models.ImageField(_("image"), upload_to=upload_to, default = 'post')
    diseaseFirst = models.TextField(max_length=50, null= True)
    treatment = models.TextField(max_length=3000, null= True)
    prevention = models.TextField(max_length=3000, null= True)
    symptoms = models.TextField(max_length=3000, null= True)
    additionalInformation = models.TextField(max_length=3000, null= True)
    probabilityDiseaseFirst = models.FloatField(null= True)
    probabilityDiseaseSecond = models.FloatField(null= True)
    diseaseSecond = models.TextField(max_length=50, null= True)

    request = models.OneToOneRel(Request, field_name="", to='')

    # Methods